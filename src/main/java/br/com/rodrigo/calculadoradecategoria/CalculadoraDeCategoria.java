/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradecategoria;

/**
 *
 * @author Diamond
 */
public class CalculadoraDeCategoria {
   public static final String INFANTIL_A = "Infantial A";
    public static final String INFANTIL_B = "Infantial B";
    public static final String JUVENIL_A = "Juvenil A";
    public static final String JUVENIL_B = "Juvenil B";
    public static final String SENIOR = "Senior";

    public String calcular(Pessoa p) {
        if (p.getIdade() >= 5 && p.getIdade() <= 7) {
            return INFANTIL_A;
        } else if (p.getIdade() <= 10) {
            return INFANTIL_B;
        } else if (p.getIdade() <= 13) {
            return JUVENIL_A;
        } else if (p.getIdade() <= 17) {
            return JUVENIL_B;
        }
        return SENIOR;
    }

}
 

