/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradecategoria;

/**
 *
 * @author Diamond
 */
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class CalculadoraDeCategoriaTest {

    private CalculadoraDeCategoria calculadora;
    private Pessoa jose;
    private Pessoa miguel;
    private Pessoa abraao;
    private Pessoa jonas;
    private Pessoa malaquias;

    @Before
    public void init() {
        System.out.println("Antes do teste...");
        calculadora = new CalculadoraDeCategoria();
        jose = new Pessoa("Jose", 5);
        miguel = new Pessoa("Miguel", 9);
        abraao = new Pessoa("Abraao", 12);
        jonas = new Pessoa("Jonas", 16);
        malaquias = new Pessoa("Malaquias", 25);
    }
    
    @After
    public void mensagem(){
        System.out.println("apos o teste ....");
    }

    @Test
    public void deveSerInfantialA() {
        assertEquals(CalculadoraDeCategoria.INFANTIL_A, calculadora.calcular(jose));
    }

    @Test
    public void deveSerInfantialB() {
        assertEquals(CalculadoraDeCategoria.INFANTIL_B, calculadora.calcular(miguel));
    }
    
     @Test
    public void deveSerJuvenilA() {
        assertEquals(CalculadoraDeCategoria.JUVENIL_A, calculadora.calcular(abraao));
    }
    
    @Test
    public void deveSerJuvenilaB(){
        assertEquals(CalculadoraDeCategoria.JUVENIL_B, calculadora.calcular(jonas));
    }
    
     
    @Test
    public void deveSerSenior(){
        assertEquals(CalculadoraDeCategoria.SENIOR, calculadora.calcular(malaquias));
    }
    
    
    

}
